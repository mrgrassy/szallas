<?php
/**
 * user: laszlobravik
 * date: 2023. 02. 23.
 * time: 23:16
 * email: braviklacy@msn.com
 */


namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Company;
use Illuminate\Http\Request;


class CompaniesController extends Controller
{
    /*
    |-------------------------------------------------------------------------------
    | Get All Companies
    |-------------------------------------------------------------------------------
    | URL:            /api/companies
    | Method:         GET
    | Description:    Gets all the companies in the application
    */
    public function getCompanies(Request $request)
    {
        if ($request->has('ids')) {
            $companyIds = explode(',', $request->get('companyIds'));
            $companies = Company::whereIn('companyId', $companyIds)->get();
            return response()->json($companies);
        } else {
            $companies = Company::all();
            return response()->json($companies);
        }
    }

    /*
    | -------------------------------------------------------------------------------
    | Get An Individual Company
    |-------------------------------------------------------------------------------
    | URL:            /api/companies/{id}
    | Method:         GET
    | Description:    Gets an individual company
    | Parameters:
    |   $id   -> ID of the company we are retrieving
    */
    public function getCompany($id)
    {
        $company = Company::where('companyId', '=', $id)->first();
        return response()->json($company);
    }

    /*
    |-------------------------------------------------------------------------------
    | Adds a New Company
    |-------------------------------------------------------------------------------
    | URL:            /api/companies
    | Method:         POST
    | Description:    Adds a new company to the application
    */
    public function postNewCompany(Request $request)
    {
        if ($request->has('companyName') && $request->has('companyRegistrationNumber')
            && $request->has('companyFoundationDate') && $request->has('country')
            && $request->has('zipCode') && $request->has('city')
            && $request->has('streetAddress') && $request->has('latitude')
            && $request->has('longitude') && $request->has('companyOwner')
            && $request->has('employees') && $request->has('activity')
            && $request->has('active') && $request->has('email')
            && $request->has('password')) {

            $company = new Company();
            $company->companyName = $request->input('companyName');
            $company->companyRegistrationNumber = $request->input('companyRegistrationNumber');
            $company->companyFoundationDate = $request->input('companyFoundationDate');
            $company->country = $request->input('country');
            $company->zipCode = $request->input('zipCode');
            $company->city = $request->input('city');
            $company->streetAddress = $request->input('streetAddress');
            $company->latitude = $request->input('latitude');
            $company->longitude = $request->input('longitude');
            $company->companyOwner = $request->input('companyOwner');
            $company->employees = $request->input('employees');
            $company->activity = $request->input('activity');
            $company->active = $request->input('active');
            $company->email = $request->input('email');
            $company->password = $request->input('password');
            $company->save();

            return response()->json($company, 201);
        } else {
            return response()->json(["error" => "All Data Required"], 400);
        }
    }

    /*
    |-------------------------------------------------------------------------------
    | Update a Company
    |-------------------------------------------------------------------------------
    | URL:            /api/company
    | Method:         PUT
    | Description:    Update a company
    */
    public function updateCompany(Request $request, $id)
    {
        if ($request->has('companyRegistrationNumber')) {
            return response()->json(["error" => "The registration date cannot be changed!"], 400);
        } else {
            $company = Company::findOrFail($id);
            $company->update($request->all());
            return response()->json($company, 200);
        }
    }
}
