<?php
/**
 * user: laszlobravik
 * date: 2023. 02. 24.
 * time: 1:07
 * email: braviklacy@msn.com
 */


namespace App\Helper;


class Helper
{
    public static function import_CSV($filename, $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;
        $header = null;
        $data = [];
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 2000, $delimiter)) !== false) {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }
}
