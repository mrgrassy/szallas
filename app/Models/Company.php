<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['companyId', 'companyName', 'companyRegistrationNumber','companyFoundationDate','country','zipCode','city','streetAddress','latitude','longitude','companyOwner','employees','activity','active','email'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    /**
     * Disable Create_At modification
     *
     * @param $value
     *
     * @return null
     */
    public function setCreatedAt($value)
    {
        return NULL;
    }
}
