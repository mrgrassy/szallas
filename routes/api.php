<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

/*
  |-------------------------------------------------------------------------------
  | Get All Companies
  |-------------------------------------------------------------------------------
  | URL:            /api/companies
  | Controller:     API\CompaniesController@getCompanies
  | Method:         GET
  | Description:    Gets all the companies in the application
  */
Route::get('/companies', 'API\CompaniesController@getCompanies');

/*
  |-------------------------------------------------------------------------------
  | Get An Individual Company
  |-------------------------------------------------------------------------------
  | URL:            /api/companies/{id}
  | Controller:     API\CompaniesController@getCompany
  | Method:         GET
  | Description:    Gets an individual company
  */
Route::get('/companies/{id}', 'API\CompaniesController@getCompany');


/*
  |-------------------------------------------------------------------------------
  | Adds a New Company
  |-------------------------------------------------------------------------------
  | URL:            /api/companies
  | Controller:     API\CompaniesController@postNewCompany
  | Method:         POST
  | Description:    Adds a new company to the application
  */
Route::post('/companies', 'API\CompaniesController@postNewCompany');

/*
  |-------------------------------------------------------------------------------
  | Update a Company
  |-------------------------------------------------------------------------------
  | URL:            /api/companies
  | Controller:     API\CompaniesController@modifyCompany
  | Method:         OUT
  | Description:    Update a company
  */
Route::put('/company/{id}',"API\CompaniesController@updateCompany");
