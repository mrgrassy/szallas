<?php

namespace Database\Seeders;

use App\Models\Company;
use Illuminate\Database\Seeder;
use App\Helper\Helper;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $table = 'company';
        $file = public_path("/seeders/$table".".csv");
        Company::truncate();
        $records = Helper::import_CSV($file,";");
        foreach ($records as $key => $record) {
            Company::create([
                'companyId' => $record['companyId'],
                'companyName' => $record['companyName'],
                'companyRegistrationNumber' => $record['companyRegistrationNumber'],
                'companyFoundationDate' => $record['companyFoundationDate'],
                'country' => $record['country'],
                'zipCode' => $record['zipCode'],
                'city' => $record['city'],
                'streetAddress' => $record['streetAddress'],
                'latitude' => $record['latitude'],
                'longitude' => $record['longitude'],
                'companyOwner' => $record['companyOwner'],
                'employees' => $record['employees'],
                'activity' => $record['activity'],
                'active' => boolval($record['active']),
                'email' => $record['email'],
                'password' => $record['password']
            ]);
        }
    }
}
