<?php

use Illuminate\Database\Migrations\Migration;
use \Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::statement("CREATE VIEW companiesPivotView AS SELECT
  MAX(CASE WHEN activity = 'Car' THEN companyName ELSE NULL END) AS Car,
  MAX(CASE WHEN activity = 'Building Industry' THEN companyName ELSE NULL END) AS Building_Industry,
  MAX(CASE WHEN activity = 'Growing Plants' THEN companyName ELSE NULL END) AS Growing_Plants,
  MAX(CASE WHEN activity = 'IT' THEN companyName ELSE NULL END) AS IT,
  MAX(CASE WHEN activity = 'Food' THEN companyName ELSE NULL END) AS Food
FROM companies
GROUP BY activity,companyName;
");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::statement("DROP VIEW companiesPivotView");
    }
};
