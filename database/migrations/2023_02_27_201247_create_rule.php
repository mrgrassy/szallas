<?php

use Illuminate\Database\Migrations\Migration;
use \Illuminate\Support\Facades\DB;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        DB::query('DENY UPDATE ON companies (created_at) TO Created_at_Rule;');
    }


    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        DB::query('GRANT UPDATE ON companies (created_at) TO Created_at_Rule');
    }
};
