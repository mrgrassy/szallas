<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id('companyId')->autoIncrement();
            $table->string('companyName');
            $table->string('companyRegistrationNumber');
            $table->date('companyFoundationDate');
            $table->string('country');
            $table->string('zipCode');
            $table->string('city');
            $table->string('streetAddress');
            $table->float('latitude');
            $table->float('longitude');
            $table->string('companyOwner');
            $table->integer('employees');
            $table->string('activity');
            $table->boolean('active');
            $table->string('email');
            $table->string('password');
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('companies');
    }
};
